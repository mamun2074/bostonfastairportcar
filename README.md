
**Project Title: Car Reservation System**

**Project Description:**

The Car Reservation System is a comprehensive web application designed to streamline the process of reserving cars online. Built with raw PHP, this project features both frontend and backend components and includes two distinct user panels: Admin and Customer.

**Key Features:**

**1. Customer Panel:**

**- User Authentication:** Customers can register and log in to their accounts securely.
**- Car Reservation:** Users can browse available cars and make reservations through an intuitive interface.

**- Payment Integration:** The system supports payments via PayPal, ensuring a seamless and secure transaction process.


**2. Admin Panel:**

**User Management:** Admins can manage customer accounts, including viewing, editing, and deleting users.

**Reservation Management:** Admins have access to all reservations, with the ability to approve, decline, or modify bookings.

**Car Management:** Admins can add, edit, and remove car listings to keep the inventory up-to-date.

**Reporting:** Generate reports on reservations, payments, and customer activities for better business insights.

**Technologies Used:**

**- Frontend:** HTML, CSS, JavaScript

**- Backend:** Raw PHP

**- Database:** MySQL

**- Payment Gateway:** PayPal API
 

**Purpose:**
This system is designed to provide a user-friendly and efficient way for customers to reserve cars online while giving admins the tools they need to manage the business effectively.

